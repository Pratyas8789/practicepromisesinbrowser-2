fetch("https://jsonplaceholder.typicode.com/users")
    .then((response) => {
        return response.json();
    })
    .then((dataOfUser) => {
        return Promise.all(dataOfUser.map((user) => getAllTods(user)))
    })
    .then((response) => {
        response.forEach((data) => {
            displayTodo(data);
        })
    })
    .catch((error) => {
        console.error(error.message);
    })

function getAllTods(user) {
    return fetch(`https://jsonplaceholder.typicode.com/todos?userId=${user.id}`)
        .then((responseOfTodo) => {
            return responseOfTodo.json();
        }).then((dataOfTodo) => {
            const objOfTodo = {
                userName: user.name,
                allTodos: dataOfTodo
            }
            return objOfTodo;
        })
        .catch((error) => {
            console.error(error.message);
        })
}