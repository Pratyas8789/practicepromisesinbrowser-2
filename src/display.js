const displayTodo = (data) => {
    const container = document.getElementById("container");
    const allTodos = data.allTodos;
    allTodos.forEach(todo => {
        const card = document.createElement("div");
        card.innerHTML += `
        <h2><input type="checkbox" name="" id="">@${data.userName + " " + "&nbsp" + "&nbsp" + "&nbsp" + "&nbsp" + todo.title}</h2>  
        `
        container.appendChild(card);
    });
}